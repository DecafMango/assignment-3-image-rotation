#include <stdlib.h>

#include "bmp.h"

#define BMP_SIGNATURE_HEX 0x4D42
#define BMP_RESERVED 0
#define BMP_SIZE 40
#define BMP_PLANES 1
#define BMP_BIT_COUNT 24
#define BMP_COMPRESSION 0
#define BMP_X_PELS_PER_METER 1
#define BMP_Y_PELS_PER_METER 1
#define BMP_CLR_USED 0
#define BMP_CLR_IMPORTANT 0

static struct bmp_header create_bmp_header(struct image const* image) {
    struct bmp_header header = {0};

    uint8_t padding = (4 - (image -> width * sizeof(struct pixel)) % 4) % 4;

    header.bfType = BMP_SIGNATURE_HEX;
    header.bfileSize = sizeof(struct bmp_header) + (sizeof(struct pixel) * image -> width + padding) * image -> height;
    header.bfReserved = BMP_RESERVED;
    header.bOffBits = sizeof(struct bmp_header);
    header.biSize = BMP_SIZE;
    header.biHeight = image -> height;
    header.biWidth = image -> width;
    header.biPlanes = BMP_PLANES;
    header.biBitCount = BMP_BIT_COUNT;
    header.biCompression = BMP_COMPRESSION;
    header.biSizeImage = (image -> width + padding) * image -> height;
    header.biXPelsPerMeter = BMP_X_PELS_PER_METER;
    header.biYPelsPerMeter = BMP_Y_PELS_PER_METER;
    header.biClrUsed = BMP_CLR_USED;
    header.biClrImportant = BMP_CLR_IMPORTANT;

    return header;
}

enum read_status from_bmp(FILE* source_file, struct image* image) {
    struct bmp_header header;

    // it's supposed to read 1 element - if fread() returns different value - error
    if (fread(&header, sizeof(struct bmp_header), 1, source_file) != 1) {
        return READ_INVALID_HEADER;
    }

    // check file signature
    if (header.bfType != BMP_SIGNATURE_HEX) {
        return READ_INVALID_SIGNATURE;
    }
    // check pixels size - it must be 24 bits
    if (header.biBitCount != BMP_BIT_COUNT) {
        return READ_INVALID_BITS;
    }

    // work with struct image

    uint32_t width = header.biWidth;
    uint32_t height = header.biHeight;

    image -> width = width;
    image -> height = height;


    image -> data = malloc(sizeof(struct pixel) * width * height);
    if (!(image -> data)) {
        free(image);
        return READ_ERROR;
    }

    uint8_t padding = (4 - (width * sizeof(struct pixel)) % 4) % 4;

    for (uint64_t y = 0; y < height; y++) {
        if (fread(&image -> data[y * width], sizeof(struct pixel), width, source_file) != width) {
            free(image -> data);
            return READ_ERROR;
        }
        fseek(source_file, padding, SEEK_CUR);
    }

    return READ_OK;
}

enum write_status to_bmp(FILE* target_file, struct image const* image) {
    struct bmp_header header = create_bmp_header(image);

    uint8_t padding = (4 - (image -> width * sizeof(struct pixel)) % 4) % 4;

    if (fwrite(&header, sizeof(struct bmp_header), 1, target_file) != 1) {
        return WRITE_ERROR;
    }

    for (uint32_t y = 0; y < image -> height; y++) {
        if (fwrite(&image -> data[image -> width * y], sizeof(struct pixel), image -> width, target_file) != image -> width) {
            return WRITE_ERROR;
        }
        fseek(target_file, padding, SEEK_CUR);
    }

    return WRITE_OK;
}
