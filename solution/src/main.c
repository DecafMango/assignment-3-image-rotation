#include <stdlib.h>

#include "bmp.h"
#include "file_open.h"
#include "image.h"
#include "parser.h"
#include "rotation.h"

enum program_status {
    PROGRAM_SUCCESS = 0,
    PROGRAM_ARGUMENTS_PARSE_ERROR,
    PROGRAM_FILE_OPEN_ERROR,
    PROGRAM_BMP_READ_ERROR,
    PROGRAM_ROTATION_ERROR,
    PROGRAM_BMP_WRITE_ERROR
};

static const char* error_messages[] = {"Usage: ./image-transformer <source-image> <transformed-image> <angle>",
                                     "Angle must be a value of [0, 90, -90, 180, -180, 270, -270]",
                                       "Angle must be an integer value",
                                       "Source file doesn't exist",
                                       "No access to files",
                                       "Invalid signature of source file",
                                       "Source image should be made from 24 byte pixels",
                                       "Invalid source file's header",
                                       "Something went wrong during source file reading",
                                       "Something went wrong during target file writing",
                                       "Something went wrong during image rotating"
                                     };

int main(int argc, char **argv) {
    // parse commandline arguments
    char *source_filename;
    char *target_filename;
    long angle;

    enum parse_status parse_result = parse(argc, argv, &source_filename, &target_filename, &angle);
    if (parse_result) {
        fprintf(stderr, "%s\n" , error_messages[parse_result - 1]);
        return PROGRAM_ARGUMENTS_PARSE_ERROR;
    }

    // open sourcefile
    FILE *source_file;

    enum file_open_status source_file_open_result = file_open(source_filename, &source_file, FILE_OPEN_READ_ONLY);
    if (source_file_open_result) {
        fprintf(stderr, "%s\n" , error_messages[source_file_open_result - 1]);
        return PROGRAM_FILE_OPEN_ERROR;
    }

    // read sourcefile
    struct image source_image = {0};
    enum read_status source_bmp_read_result = from_bmp(source_file, &source_image);
    if (source_bmp_read_result) {
        fclose(source_file);
        fprintf(stderr, "%s\n" , error_messages[source_bmp_read_result - 1]);
        return PROGRAM_BMP_READ_ERROR;
    }

    // close sourcefile
    fclose(source_file);

    // rotate source image
    struct image target_image = {0};
    enum rotation_status rotation_result = rotate(&source_image, &target_image, angle);
    if (rotation_result) {
        free(source_image.data);
        free(target_image.data);
        fprintf(stderr, "%s\n" , error_messages[rotation_result - 1]);
        return PROGRAM_ROTATION_ERROR;
    }

    // open target file
    FILE* target_file;
    enum file_open_status target_file_open_result = file_open(target_filename, &target_file, FILE_OPEN_READ_AND_WRITE);
    if (target_file_open_result) {
        free(source_image.data);
        free(target_image.data);
        fprintf(stderr, "%s\n" , error_messages[target_file_open_result - 1]);
        return PROGRAM_FILE_OPEN_ERROR;
    }

    // write to target file
    enum write_status target_bmp_write_result = to_bmp(target_file, &target_image);
    if (target_bmp_write_result) {
        fclose(target_file);
        free(source_image.data);
        free(target_image.data);
        fprintf(stderr, "%s\n" , error_messages[target_bmp_write_result - 1]);
        return PROGRAM_BMP_WRITE_ERROR;
    }

    // close target file and free allocated memory
    fclose(target_file);
    free(source_image.data);
    free(target_image.data);

    return PROGRAM_SUCCESS;
}
