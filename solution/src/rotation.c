#include <stdlib.h>

#include "rotation.h"

static enum rotation_status copy_image(struct image const* source_image, struct image* target_image) {
    target_image -> width = source_image -> width;
    target_image -> height = source_image -> height;

    target_image -> data = malloc(sizeof(struct pixel) * target_image -> width * target_image -> height);
    if (!target_image -> data) {
        return ROTATION_ERROR;
    }

    for (uint32_t y = 0; y < target_image -> height; y++) {
        for (uint32_t x = 0; x < target_image -> width; x++) {
            target_image -> data[target_image -> width * y + x] =
                    source_image -> data[source_image -> width * y + x];
        }
    }

    return ROTATION_OK;
}

static void rotate_90deg(struct image const* source_image, struct image* target_image) {
    target_image -> width = source_image -> height;
    target_image -> height = source_image -> width;

    for (uint32_t y = 0; y < target_image -> height; y++) {
        for (uint32_t x = 0; x < target_image -> width; x++) {
            target_image -> data[target_image -> width * y + x] =
                    source_image -> data[source_image -> width * x + (source_image -> width - y - 1)];
        }
    }
}


enum rotation_status rotate(struct image const* source_image, struct image* target_image, long angle) {

    if (copy_image(source_image, target_image)) {
        return ROTATION_ERROR;
    }

    // this variable shows how many times we have to rotate our image on 90deg
    long times = angle >= 0 ? angle / 90 : 4 + angle / 90;

    for (long i = 0; i < times; i++) {
        struct image intermediate_image;
        if (copy_image(target_image, &intermediate_image)) {
            return ROTATION_ERROR;
        }
        rotate_90deg(&intermediate_image, target_image);
        free(intermediate_image.data);
    }

    return ROTATION_OK;
}
