#include <errno.h>

#include "file_open.h"

enum file_open_status file_open(char* filename, FILE** file, enum file_open_type type) {
    switch (type) {
        case FILE_OPEN_READ_ONLY:
            *file = fopen(filename, "rb");
            break;
        case FILE_OPEN_READ_AND_WRITE:
            *file = fopen(filename, "wb");
    }

    // check errors
    if (!*file) {
        switch (errno) {
            case EACCES:
                return FILE_OPEN_NO_ACCESS;
            case ENOENT:
                return FILE_OPEN_NOT_EXIST;
            default:
                fprintf(stderr, "Something went wrong during file %s opening", filename);
        }
    }

    return FILE_OPEN_OK;
}


