#include <stdlib.h>

#include "parser.h"

#define PROGRAM_ARGUMENT_COUNT 4

enum parse_status parse(int argc, char** argv, char** source_filename, char** target_filename, long* angle) {
    // check arguments count
    if (argc != PROGRAM_ARGUMENT_COUNT) {
        return PARSE_NOT_REQUIRED_ARGUMENTS_COUNT;
    }

    // check angle is integer
    char* angle_str = argv[3];
    char* err_ptr;
    *angle = strtol(angle_str, &err_ptr, 10);

    // if angle is not a number err_ptr will point to non-numeric character in angle_ptr else it will point to zero-terminator
    if (*err_ptr != '\0') {
        return PARSE_NON_NUMERIC_ANGLE;
    }

    // check angle is value from list [0, 90, -90, 180, -180, 270, -270]

    if (*angle >= -270 && *angle <= 270) {
        if (*angle % 90 != 0) {
            return PARSE_PROHIBITED_ANGLE;
        }
    } else {
        return PARSE_PROHIBITED_ANGLE;
    }

    *source_filename = argv[1];
    *target_filename = argv[2];

    return PARSE_OK;
}
