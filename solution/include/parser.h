#ifndef PARSER_H
#define PARSER_H

enum parse_status {
    PARSE_OK = 0,
    PARSE_NOT_REQUIRED_ARGUMENTS_COUNT = 1,
    PARSE_NON_NUMERIC_ANGLE = 2,
    PARSE_PROHIBITED_ANGLE = 3
};

enum parse_status parse(int argc, char** argv, char** source_filename, char** target_filename, long* angle);

#endif
